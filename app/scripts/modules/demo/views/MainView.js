/**
* View example with data binding
*
* @author rkalms <rkalms@vertic.com>
*/

/* global define */

define(['jquery', 'backbone', 'underscore'], function ($, Backbone, _) {
	'use strict';

	var MainView = Backbone.View.extend({
		el: '#content-frame',

		events: {
			'submit': '_setVal'
		},

		initialize: function () {
			this.model.on('change', this.render.bind(this));
		},

		render: function () {
			//
			this.$el.prev().text(this.model.get('title'));
			this.$el.append('<h2>The model changed!</h2>');
		},

		_setVal: function (e) {
			this.model.set('title', this.$el.find('input').val());

			e.preventDefault();
		}
	});

	return MainView;
});
