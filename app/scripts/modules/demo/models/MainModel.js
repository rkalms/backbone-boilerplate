/**
* Model example
* @author rkalms <rkalms@vertic.com>
*/

/* global define */

define(['jquery', 'backbone', 'underscore'], function ($, Backbone, _) {
	'use strict';

	var MainModel = Backbone.Model.extend({});

	return MainModel;
});
