/*global define */

define(['./views/MainView', './models/MainModel'], function (MainView, MainModel) {
	'use strict';

	return function () {

		// This function will be the initialization function
		// everytime this bundle is requested from the Router
		//
		// So add all your logic here. Eg: loading views, models, setup events etc..

		var mainView = new MainView({
			model: new MainModel({
				title: ''
			})
		});
	};
});
