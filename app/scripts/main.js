/*global require*/
'use strict';

require.config({
	shim: {
		underscore: {
			exports: '_'
		},
		backbone: {
			deps: [
				'underscore',
				'jquery'
			],
			exports: 'Backbone'
		},
		handlebars: {
			deps: [
				'underscore',
				'jquery'
			],
			exports: 'Handlebars'
		}
	},
	paths: {
		jquery: '../components/jquery/jquery',
		backbone: '../components/backbone-amd/backbone',
		underscore: '../components/underscore-amd/underscore',
		handlebars: '../components/handlebars/handlebars.min'
	}
});

require([
	'app'
], function (App) {
	window.App = App;

	App.start();
});
