/**
* Router
*
* @author rkalms
*/

/*global define*/

define([
	'jquery',
	'underscore',
	'backbone',
	'router'
], function ($, _, Backbone, router) {
	'use strict';

	// Add your modules routing here.
	router.route('/*', 'home', function () {
		this.loadModule('modules/demo/main');
	});

	var root = $('[data-main][data-root]').data('root');

	root = root ? root : '/';

	return {
		start: function () {
			Backbone.history.start({
				pushState: true,
				root: root
			});
		}
	};
});
